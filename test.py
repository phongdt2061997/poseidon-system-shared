import logging
from common.logs import LogService

DEBUG = True
LOG_LEVEL = logging.DEBUG if DEBUG else logging.INFO
import logging
from common.logs import LogService

logger = LogService(service_name="gg-search-service", log_level=logging.DEBUG, console_output=True)

def print_error(message, **extra_data):
    logger.log(message=message, level=logging.ERROR, **extra_data)

def print_info(message, **extra_data):
    logger.log(message=message, level=logging.INFO, **extra_data)

def print_warning(message, **extra_data):
    logger.log(message=message, level=logging.WARNING, **extra_data)

def print_exception(message, exception, **extra_data):
    logger.exception(message=message, exce=exception, **extra_data)

try:
    a = 1 / 0
except Exception as e:
    print_exception("Error", exception=e, a=1, b=2, c=3)
    print_info("Info")
    print_warning("Warning")