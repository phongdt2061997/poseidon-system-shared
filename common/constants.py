from enum import Enum

class RedisTopic(str, Enum):
    REDIS_CRAWLED_ADS_POSTS_TOPIC = "ads_posts:crawled" # Lưu trữ những ad-post được BOT cào về
    REDIS_UNIQUED_ADS_POSTS_TOPIC = "ads_posts:uniqued" # Lưu trữ ad-post id mục đích lọc trùng
    
    REDIS_UNIQUED_PRODUCT_URLS_TOPIC = "ads_posts:product_urls" # Lưu trữ danh sách product_urls (đã gán mã SP) mục đích lọc trùng
    REDIS_UNIQUED_JUNK_URLS_TOPIC = "ads_posts:junk_urls" # Lưu trữ danh sách junk_urls (Loại bỏ) mục đích lọc trùng
    
    REDIS_SET_FAVORITED_DOMAINS_TOPIC = "fdomains:crawled:set" # Lưu trữ tạm danh sách f-domains (giảm thiểu việc query đến DB)
    REDIS_LIST_FAVORITED_DOMAINS_TOPIC = "fdomains:crawled:list" # Lưu trữ danh sách f-domains cho BOT lấy về crawl

    # need to update status of domains
    REDIS_SET_NEED_UPDATE_DOMAINS_TOPIC = "domains:need_update:set" # Lưu trữ tạm danh sách domains cần được update trạng thái (Mục đích lọc trùng)
    REDIS_LIST_NEED_UPDATE_DOMAINS_TOPIC = "domains:need_update:list" # Lưu trữ danh sách domains cần được update trạng thái

    REDIS_UNIQUED_DOMAINS_TOPIC = "domains:uniqued" # Lưu trữ domains mục đích lọc trùng
    REDIS_CRAWLED_DOMAINS_TOPIC = "domains:crawled" # Lưu những domains mà BOT mới cào được 
    REDIS_BLACKLIST_DOMAINS_TOPIC = "domains:blacklist" # Lưu những blacklist domains 
    
    REDIS_SET_KEYWORDS_TOPIC = "keywords:set" # Lưu trữ tạm danh sách keywords (giảm thiểu query DB)
    REDIS_LIST_KEYWORDS_TOPIC = "keywords:list" # Lưu trữ danh sách keywords cho BOT lấy về crawl
    
    REDIS_PROFILES_FOR_BOT_TOPIC = "profiles:bot_crawler" # Lưu trữ danh sách profile dành riêng cho BOT chạy

    REDIS_QUEUE_FOR_PRODUCT_IMAGES_ALIBABA = "alibaba:product_images:list" # Lưu trữ danh sách sản phẩm cho BOT CRAWLER chạy
    REDIS_QUEUE_FOR_CRAWLED_IMAGES_ALIBABA = "alibaba:crawled_images:list" # Lưu trữ danh sách hình ảnh mà BOT CRAWLER đã chạy được
    REDIS_QUEUE_FOR_TRAINING_IMAGES_ALIBABA = "alibaba:training_images:list" # Lưu trữ danh sách hình ảnh để google vision train data
    
    REDIS_PREDICTED_PRODUCT = "products:predicted:list" # Lưu trữ kết quả tìm kiểm sản phẩm bằng google AI đã được train data

    REDIS_IMAGE_CRAWLER_BOT_NAME = "bot:ali_img_crawler"
    
class KafkaTopic(str, Enum):
    KAFKA_ADS_POSTS_CRAWLED_TOPIC = "ads-posts-crawled-topic"
    KAFKA_ADS_POSTS_ACCEPTED_TOPIC = "ads-posts-accepted-topic"
    KAFKA_DOMAINS_FAVORITED_TOPIC = "domains-favorited-topic"
    KAFKA_DOMAINS_TOPIC = "domains-topic"
    KAFKA_GOOGLE_LENS_SEARCH_TOPIC = "google-lens-search-topic"
    KAFKA_ALIBABA_IMAGES_CRAWLED_TOPIC = "alibaba-images-crawled"
    KAFKA_GOOGLE_VISION_FINE_TUNING_PRODUCTS = "google-vision-fine-tuning-products"
    KAFKA_UPDATE_DOMAIN_OF_PRODUCT = "update-domain-of-product-topic"