
import os
import json
import logging
import traceback
from logging.handlers import RotatingFileHandler
from datetime import datetime

class CustomRotatingFileHandler(RotatingFileHandler):
    """
    使日志分片名称不再放到后缀中

    RotatingFileHandler: aaa.log.1
    CustomRotatingFileHandler: aaa_1.log
    """

    def doRollover(self):
        """
        Do a rollover, as described in __init__().
        """
        if self.stream:
            self.stream.close()
            self.stream = None
        if self.backupCount > 0:
            for i in range(self.backupCount - 1, 0, -1):
                sfn = self._get_new_file_name(i)
                dfn = self._get_new_file_name(i + 1)
                if os.path.exists(sfn):
                    if os.path.exists(dfn):
                        os.remove(dfn)
                    os.rename(sfn, dfn)
            dfn = self._get_new_file_name(1)
            if os.path.exists(dfn):
                os.remove(dfn)
            os.rename(self.baseFilename, dfn)
        if not self.delay:
            self.stream = self._open()

    def _get_new_file_name(self, index):
        """
        Generate a new file name with the index inserted before the extension.
        """
        base, ext = os.path.splitext(self.baseFilename)
        return f"{base}_{index}{ext}"

class JSONFormatter(logging.Formatter):

    def __init__(self):
        super().__init__()

    def format(self, record):
        log_record = {
            "timestamp": datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
            "level": record.levelname,
            "message": record.getMessage(),
        }
        if hasattr(record, 'extra_data'):
            log_record["extra_data"] = record.extra_data

        return json.dumps(log_record)

class LogService:
    def __init__(self, service_name, log_level=logging.INFO, console_output=False):
        self.name = service_name
        self.log_level = log_level
        self.logger = logging.getLogger(service_name)
        self.logger.setLevel(log_level)

        log_directory = os.path.join(os.getcwd(), 'logs')
        if not os.path.exists(log_directory):
            os.makedirs(log_directory)

        log_file = self._get_log_file()
        file_handler = CustomRotatingFileHandler(log_file, maxBytes=20 * 1024 * 1024, backupCount=20, encoding='utf-8')
        formatter = JSONFormatter()
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

        if console_output:
            console_handler = logging.StreamHandler()
            console_handler.setFormatter(formatter)
            self.logger.addHandler(console_handler)

    def _get_log_file(self):
        today = datetime.now().strftime('%Y-%m-%d')
        log_file = f"{self.name}_{today}_%s.log" % logging.getLevelName(self.log_level)
        log_path = os.path.join('logs', log_file)
        return log_path

    def log(self, message, level=logging.INFO, **extra_data):
        extra_info = {}
        if extra_data:
            extra_info = {'extra_data': extra_data}

        if level == logging.DEBUG:
            self.logger.debug(message, extra=extra_info)
        elif level == logging.INFO:
            self.logger.info(message, extra=extra_info)
        elif level == logging.WARNING:
            self.logger.warning(message, extra=extra_info)
        elif level == logging.ERROR:
            self.logger.error(message, extra=extra_info)
        elif level == logging.CRITICAL:
            self.logger.critical(message, extra=extra_info)

    def exception(self, message, exce, **extra_data):
        self.log(f"{message}: {self.get_traceback_string(exce)}", logging.ERROR, **extra_data)

    def get_traceback_string(self, exception):
        traceback_list = traceback.format_exception(type(exception), exception, exception.__traceback__)
        traceback_str = ''.join(traceback_list)
        return traceback_str
